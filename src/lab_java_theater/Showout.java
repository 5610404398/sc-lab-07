package lab_java_theater;

import java.util.ArrayList;

public class Showout {
	SeatPrice sp = new SeatPrice();
	TheaterManage tm = new TheaterManage();
	
	
	public void showEmptyseat(int d,int t) {
		int count = 0;
		ArrayList<String> ans = sp.lookEmp(d,t);
		for (String x : ans) {
			if (count == 14) {
				count = 0;
				System.out.println(x);
			} else {
				System.out.print(x + " ");
				count++;
			}
		}
	}
	
	public void showEmptyseat_Price(int d , int t,int price) {
		int count = 0;
		ArrayList<String> ans = sp.emptySeat(d,t,price);
		for (String x : ans) {
			if (count == 14) {
				count = 0;
				System.out.println(x);
			} else {
				System.out.print(x + " ");
				count++;
			}
		}
	}

}
