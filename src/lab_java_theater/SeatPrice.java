package lab_java_theater;

import java.util.ArrayList;

public class SeatPrice {
	ArrayList<String> empty = new ArrayList<String>();
	ArrayList<String> lookemp = new ArrayList<String>();
	
	double cost;
	DataSeatPrice dts = new DataSeatPrice();
	double[][] d1t1 = dts.importDataPrice();
	double[][] d1t2 = dts.importDataPrice();
	double[][] d2t1 = dts.importDataPrice();
	double[][] d2t2 = dts.importDataPrice();
	double[][] d2t3 = dts.importDataPrice();

	String[][] s11 = dts.importstatus();
	String[][] s12 = dts.importstatus();
	String[][] s21 = dts.importstatus();
	String[][] s22 = dts.importstatus();
	String[][] s23 = dts.importstatus();

	public double bookingSeat(int day, int time, int vr, int vc) {
		if (day == 1 && time == 1) {
			double cost = d1t1[vr][vc];
			d1t1[vr][vc] = 0;
		} else if (day == 1 && time == 2) {
			double cost = d1t2[vr][vc];
			d1t2[vr][vc] = 0;
		} else if (day == 2 && time == 1) {
			double cost = d2t1[vr][vc];
			d2t1[vr][vc] = 0;
		} else if (day == 2 && time == 2) {
			double cost = d2t2[vr][vc];
			d2t2[vr][vc] = 0;
		} else if (day == 2 && time == 3) {
			double cost = d2t3[vr][vc];
			d2t3[vr][vc] = 0;
		}
		return cost;
	}

	public ArrayList<String> emptySeat(int day, int time, int price) {
		empty.clear();
		for (int i = 0; i < 13; i++) {
			for (int j = 0; j < 20; j++) {
				if ((day == 1 && time == 1)
						&& (d1t1[i][j] == price && d1t1[i][j] != 0)) {
					empty.add(s11[i][j]);
				} else if ((day == 1 && time == 2)
						&& (d1t2[i][j] == price && d1t2[i][j] != 0)) {
					empty.add(s12[i][j]);
				} else if ((day == 2 && time == 1)
						&& (d2t1[i][j] == price && d2t1[i][j] != 0)) {
					empty.add(s21[i][j]);
				} else if ((day == 2 && time == 2)
						&& (d2t2[i][j] == price && d2t2[i][j] != 0)) {
					empty.add(s22[i][j]);
				} else if ((day == 2 && time == 3)
						&& (d2t3[i][j] == price && d2t3[i][j] != 0)) {
					empty.add(s23[i][j]);
				}
			}
		}
		return empty;

	}

	public ArrayList<String> lookEmp(int day, int time) {
		lookemp.clear();
		for (int i = 0; i < 13; i++) {
			for (int j = 0; j < 20; j++) {
				if ((day == 1 && time == 1)
						&& (d1t1[i][j] != 0 && d1t1[i][j] != -1)) {
					lookemp.add(s11[i][j]);
				} else if ((day == 1 && time == 2)
						&& (d1t2[i][j] != 0 && d1t2[i][j] != -1)) {
					lookemp.add(s12[i][j]);
				} else if ((day == 2 && time == 1)
						&& (d2t1[i][j] != 0 && d2t1[i][j] != -1)) {
					lookemp.add(s21[i][j]);
				} else if ((day == 2 && time == 2)
						&& (d2t2[i][j] != 0 && d2t2[i][j] != -1)) {
					lookemp.add(s22[i][j]);
				} else if ((day == 2 && time == 3)
						&& (d2t3[i][j] != 0 && d2t3[i][j] != -1)) {
					lookemp.add(s23[i][j]);
				}
			}
		}
		return lookemp;
	}

}
